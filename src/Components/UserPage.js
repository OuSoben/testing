import axios from "axios";
import React, { useEffect, useState } from "react";
import { GET_ALL_USERS } from "../service/userSevice";
import UserCard from "./UserCard";
import PlaceholderCard from "./PlaceHolderCard";

const UserPage = () => {
  const [Users, setUsers] = useState([]);
  useEffect(() => {
    GET_ALL_USERS()
      .then((response) => setUsers(response))
      .catch((error) => console.log("Error : s", error));
  }, []);
  console.log("All user : ", Users);
  return (
     
     
    <div className="container  mt-5">
      <PlaceholderCard/>
      
      <h1 className="text-center">UserPage </h1>
      <div className="row">
          {
            Users.map((user) => (
               <div className="col-4 d-flex justify-content-center">
            <UserCard userInfo={user}/>
          </div>
            ))
          }
      </div>
    </div>
  );
};
export default UserPage;
