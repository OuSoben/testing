import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import {NavLink} from 'react-router-dom'

function UserCard({ userInfo }) {
  return (
    <Card style={{ width: "18rem" }}>
      <Card.Img
        variant="top"
        src={
          userInfo.avatar
            ? userInfo.avatar
            : "https://t3.ftcdn.net/jpg/02/48/42/64/360_F_248426448_NVKLywWqArG2ADUxDq6QprtIzsF82dMF.jpg"
        }
        // onError={(e)}
      />
      <Card.Body>
        <Card.Title>{userInfo.name} <span className="text-warning">customer</span></Card.Title>
        <Card.Text>{userInfo.email}</Card.Text>
        <NavLink to={'/user/${userInfo.id'}>
           <Button variant="primary">View</Button>
        </NavLink>
       
      </Card.Body>
    </Card>
  );
}

export default UserCard;
