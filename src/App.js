import logo from './logo.svg';
import './App.css';
import HomePage from './Components/HomePage';
import {BrowserRouter, Route, Routes} from "react-router-dom"
import UserPage from './Components/UserPage';
import ProductPage from './Components/ProductPage';
import NotfoundPage from './Components/NotfoundPage';
import { useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import MyNavBar from './Components/MyNavBar';


function App() {
  return (
     
    <BrowserRouter> 
    <MyNavBar/>
    <Routes>  
    
      <Route path='/' index element={<HomePage/>}/>
      <Route path='/user' element={<UserPage/>}/>
      <Route path='/product' element={<ProductPage/>}/>
       <Route path='*'element ={<NotfoundPage/>}/>

    </Routes>
        
        
    </BrowserRouter>
  );
}

export default App;
