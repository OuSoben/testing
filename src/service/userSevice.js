import { api } from "../utils/api";

// get all users 
export const GET_ALL_USERS = async()=> {
      const response = await api.get("users")
      return response.data
}
// get user by id 
// create user
// update user 
// delete user 